Source: whysynth
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Dennis Braun <d_braun@kabelmail.de>
Build-Depends:
 debhelper-compat (= 12),
 dssi-dev,
 ladspa-sdk,
 libcairo2-dev,
 libasound2-dev,
 libfftw3-dev,
 liblo-dev,
 pkg-config,
Standards-Version: 4.5.0
Homepage: https://github.com/smbolton/whysynth
Vcs-Git: https://salsa.debian.org/multimedia-team/whysynth.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/whysynth
Rules-Requires-Root: no

Package: whysynth
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: qjackctl, vkeybd, dssi-host-jack
Provides: dssi-plugin
Description: DSSI Soft Synth Interface
 Disposable Soft Synth Interface (DSSI).  A brief list of features:
   - 4 oscillators, 2 filters, 3 LFOs, and 5 envelope generators per voice.
   - 10 oscillator modes minBLEP, wavecycle, asynchronous granular, three
     FM modes, waveshaper, noise, PADsynth, and phase distortion.
   - 6 filter modes.
   - flexible modulation and mixdown options.
 DSSI is a plugin API for software instruments (soft synths) with user
 interfaces, permitting them to be hosted in-process by audio applications.
 More information on DSSI can be found at: http://dssi.sourceforge.net/
